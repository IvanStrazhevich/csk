package carsale;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Busik on 4/11/17.
 */
public class ToMD5Test {
    @Test
    public void conversionCheck(){
        ToMD5 toMD5 = new ToMD5("Bu");
        String result = toMD5.convertToMD5();
        assertTrue("Результат (" + result +"(не равен 4682ea748b744d6208e982d032ded00d", result.equals("4682ea748b744d6208e982d032ded00d"));

    }
}
