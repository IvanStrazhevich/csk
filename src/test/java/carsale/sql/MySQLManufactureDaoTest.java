package carsale.sql;

import carsale.dao.DAOException;

import carsale.domain.Manufacture;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * Created by Busik on 4/11/17.
 */
public class MySQLManufactureDaoTest {

    @Test
    public void insertTest() {
        try {
            MySQLDaoFactory daoFactory = new MySQLDaoFactory();
            Manufacture manufacture = new Manufacture();
            manufacture.setCarBrand("Aston Martin");
            MySQLManufactureDao mySQLManufactureDao = new MySQLManufactureDao(daoFactory.getTestSQLConnection());
            mySQLManufactureDao.insert(manufacture);
            mySQLManufactureDao.close();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void getAllTest(){
        try{
            MySQLDaoFactory daoFactory = new MySQLDaoFactory();
            MySQLManufactureDao mySQLManufactureDao = new MySQLManufactureDao(daoFactory.getTestSQLConnection());
            List<Manufacture> list = mySQLManufactureDao.getAll();
            assertNotNull(list);
            mySQLManufactureDao.close();
        } catch (DAOException e){
            e.printStackTrace();
        }
    }
    @Test
    public void updateTest() {
        try {
            MySQLDaoFactory daoFactory = new MySQLDaoFactory();
            MySQLManufactureDao mySQLManufactureDao = new MySQLManufactureDao(daoFactory.getTestSQLConnection());
            Manufacture manufacture = new Manufacture();
            manufacture.setCarBrand("Aston Martin");
            mySQLManufactureDao.insert(manufacture);
            manufacture.setCarBrand("Honda");
            mySQLManufactureDao.update(manufacture);
            String result = manufacture.getCarBrand();
            assertTrue("Результат (" + result + ") не равен Honda", result.equals("Honda"));
            mySQLManufactureDao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void readTest() {
        int id;
        try {
            MySQLDaoFactory daoFactory = new MySQLDaoFactory();
            MySQLManufactureDao mySQLManufactureDao = new MySQLManufactureDao(daoFactory.getTestSQLConnection());
            Manufacture manufacture = new Manufacture();
            manufacture.setCarBrand("Aston Martin");
            mySQLManufactureDao.insert(manufacture);
            id = manufacture.getId();
                manufacture = mySQLManufactureDao.read(id);
                String result = manufacture.getCarBrand();
                assertTrue("Результат (" + result + ") не равен Aston Martin", result.equals("Aston Martin"));
                mySQLManufactureDao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public  void deleteTest() {
        int id;
        try {
            MySQLDaoFactory daoFactory = new MySQLDaoFactory();
            MySQLManufactureDao mySQLManufactureDao = new MySQLManufactureDao(daoFactory.getTestSQLConnection());
            Manufacture manufacture = new Manufacture();
            manufacture.setCarBrand("Aston Martin");
            mySQLManufactureDao.insert(manufacture);
            id = manufacture.getId();
                manufacture.setId(id);
                mySQLManufactureDao.delete(manufacture);
                mySQLManufactureDao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
