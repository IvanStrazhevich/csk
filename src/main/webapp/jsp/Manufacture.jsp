<%@ page contentType = "text/html;charset=utf-8" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.util.*" %>
<%@ page import="carsale.domain.Manufacture" %>
<html>
    <head>
        <style>
            h1{

                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: slateblue;

            }
            <% if(request.getAttribute("Wrong input data")!=null)
        out.println("input[type=\"text\"]::-webkit-input-placeholder {"+
            "color: red;"+
        "}"+
        "input[type=\"text\"]::-moz-placeholder {"+
            "color: red;"+
        "}"
        );%>
            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
            background-color: mediumslateblue;
            }

            li {
                float: left;
            }

            li a {
                display: block;
                color: white;
                text-align: center;
                padding: 16px;
                text-decoration: none;
            }

            li a:hover {
                background-color: darkslateblue;
            }
        </style>
        <title>
            CarSaleKitSample
        </title>
    </head>
 
    <body>
    <div style="background-color: slateblue" >
    <h1>
    <ul>
        <li><a href="CarOrder?action=Show+Car+Orders">Car Orders</a></li>
        <li><a href="Manufacture?action=Show+Manufactures" style="background-color: slateblue;" >Manufactures</a></li>
        <li><a href="Car?action=Show+Cars">Cars</a></li>
    </ul>
    </h1>
<h1>
<table>

    <tr>
        <th>Id</th>
        <th>Manufacture</th>
    </tr>
     <% for (Manufacture m: (List<Manufacture>)request.getAttribute("ManufactureList")) {
                out.println("<tr>" +
                        "<form action=\"Manufacture\">"+
                        "<td>"+m.getId()+"</td>"+
                        "<td >"+m.getCarBrand()+"</td>"+
                        "<input type=\"hidden\"name=\"id\"value=\""+m.getId()+"\">"+
                        "<input type=\"hidden\"name=\"action\"value=\"Remove Manufacture\">" +
                        "<td><input type=\"submit\" style=\"color: darkslateblue\" value=\"Remove\"> </td>" +
                        "</form>" +
                "</tr>");
                }%>
</table>

</h1>
<form action="Manufacture">
    <input type="hidden" name ="action" value="Add Manufacture">
    <input type="text" size = "30" name ="carBrand" placeholder="insert Car Brand <% if(request.getAttribute("Wrong input data")!=null){out.println( request.getAttribute("Wrong input data"));}%>">
    <input type="submit" style="color: darkslateblue" value="Add Manufacture">
</form>
<form action="MainPage">
<input type="hidden" name ="action" value="To MainPage">
  <input type="submit" style="color: darkslateblue" value="To Main Page">
</form>
    </div>
    </body>
</html>

