<%@ page contentType = "text/html;charset=utf-8" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.util.*" %>
<%@ page import="carsale.domain.Car" %>
<%@ page import="carsale.domain.Manufacture" %>
<html>
<head>
    <title>
        CarSaleKitSample
    </title>
    <style>
        h1{

            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: slateblue;

        }
        h2{

            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: slateblue;
            color: red;

        }


        <% if(request.getAttribute("Wrong input data")!=null)
        out.println("input[type=\"text\"]::-webkit-input-placeholder {"+
            "color: red;"+
        "}"+
        "input[type=\"text\"]::-moz-placeholder {"+
            "color: red;"+
        "}"
        );%>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: mediumslateblue;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: darkslateblue;
        }
    </style>

</head>

<body>
<div style="background-color: slateblue" >
    <h1>

        <ul>
            <li><a href="CarOrder?action=Show+Car+Orders">Car Orders</a></li>
            <li><a href="Manufacture?action=Show+Manufactures"  >Manufactures</a></li>
            <li><a href="Car?action=Show+Cars" style="background-color: slateblue;">Cars</a></li>
        </ul>

    </h1>
    <% if(request.getAttribute("Wrong input data")==null){
        Car car = (Car)request.getAttribute("Car To Edit");
        Manufacture manufacture = (Manufacture)request.getAttribute("Car_Brand");

        out.println("<form action=\"Car\">"+
                "Model <br>"+
                "<input type=\"text\" size = \"30\" name =\"Model\" value =\""+ car.getModel() + "\" placeholder=\"enter Model\"><br>"+
                "Power <br>"+
                "<input type=\"text\" size = \"30\" name =\"Power\" value =\""+ car.getPower() + "\"placeholder=\"enter Power kWt\"><br>"+
                "Range <br>"+
                "<input type=\"text\" size = \"30\" name =\"Range\" value =\""+ car.getRangeKm() + "\"placeholder=\"enter Range_km\"><br>"+
                "Price <br>"+
                "<input type=\"text\" size = \"30\" name =\"Price\" value =\""+ car.getPrice() + "\"placeholder=\"enter Price $\"><br>"+
                "Quantity <br>"+
                "<input type=\"text\" size = \"30\" name =\"Quantity\" value =\""+ car.getQuantity() + "\"placeholder=\"enter Quantity\"><br>"+
                "<input type=\"hidden\" name =\"Car_Brand\" value =\""+ manufacture.getCarBrand()+"\">"+
                "<input type=\"hidden\" name =\"id\" value =\""+ car.getId() +"\">"+
                "<input type=\"hidden\" name =\"action\" value=\"Edit Car\">"+
                "<input type=\"submit\" style=\"color: darkslateblue\" value=\"Edit Car\">"+
                "</form>");
    } else {


    String text = (String)request.getAttribute("Wrong input data");

    out.println("<form action=\"Car\">"+
            "<h2>"+
            "Ошибка при вводе <br>"+
            "</h2>"+
            "Model <br>"+
            "<input type=\"text\" size = \"30\" name =\"Model\" value =\""+ request.getParameter("Model") + "\" placeholder=\"enter Model "+ text +"\"><br>"+
            "<input type=\"hidden\" name =\"Car_Brand\" value =\""+ request.getParameter("Car_Brand") +"\">"+
            "Power <br>"+
            "<input type=\"text\" size = \"30\" name =\"Power\" value =\""+ request.getParameter("Power") + "\"placeholder=\"enter Power kWt "+ text + "\"><br>"+
            "Range <br>"+
            "<input type=\"text\" size = \"30\" name =\"Range\" value =\""+ request.getParameter("Range") + "\"placeholder=\"enter Range_km "+ text + "\"><br>"+
            "Price <br>"+
            "<input type=\"text\" size = \"30\" name =\"Price\" value =\""+ request.getParameter("Price") + "\"placeholder=\"enter Price $ "+ text + "\"><br>"+
            "Quantity <br>"+
            "<input type=\"text\" size = \"30\" name =\"Quantity\" value =\""+ request.getParameter("Quantity") + "\"placeholder=\"enter Quantity "+ text + "\"><br>"+
            "<input type=\"hidden\" name =\"id\" value =\""+ request.getParameter("id") +"\">"+
            "<input type=\"hidden\" name =\"action\" value=\"Edit Car\">"+
            "<input type=\"submit\" style=\"color: darkslateblue\" value=\"Edit Car\">"+
            "</form>");
}
    %>
    <form action="MainPage">
        <input type="hidden" name ="action" value="To MainPage">
        <input type="submit" style="color: darkslateblue" value="To Main Page">
    </form>
</div>
</body>
</html>

