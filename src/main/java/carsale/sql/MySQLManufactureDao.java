package carsale.sql;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import carsale.dao.DAOException;
import carsale.dao.ManufactureDao;
import carsale.domain.Manufacture;

/**
 * Created by Busik on 1/4/17.
 */
public class MySQLManufactureDao implements ManufactureDao {
    private final Connection connection;
    private PreparedStatement readpstm = null;
    private PreparedStatement deletepstm = null;
    private PreparedStatement updatepstm = null;
    private PreparedStatement getAllpstm = null;
    private PreparedStatement insertpstm = null;

    protected  void createInsertStatement() throws SQLException {
        String sql = "insert into Manufacture (Car_Brand) value (?);";
        try {
            insertpstm = connection.prepareStatement(sql,insertpstm.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeInsertStatement(Manufacture manufacture) throws SQLException {
         int id;
        try {
            insertpstm.setString(1, manufacture.getCarBrand());
            int count = insertpstm.executeUpdate();
            ResultSet rs = insertpstm.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                throw new DAOException("fail on getting idex");
            }
            manufacture.setId(id);
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }

    }

    protected void createDeleteStatement() throws SQLException {
        String sql = "DELETE FROM manufacture WHERE id= ?;";
        try {
            deletepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeDeleteStatement(Manufacture manufacture) throws SQLException {
        try {
            deletepstm.setInt(1,manufacture.getId());
            int count = deletepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected  void createUpdateStatement() throws SQLException {
        String sql = "UPDATE MANUFACTURE SET CAR_BRAND = ? WHERE ID = ?;";
        try {
            updatepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeUpdateStatement(Manufacture manufacture) throws SQLException {
        try {
            updatepstm.setString(1, manufacture.getCarBrand());
            updatepstm.setInt(2, manufacture.getId());
            int count = updatepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    protected void createReadStatement() throws SQLException {
        String sql = "SELECT ID, CAR_BRAND FROM MANUFACTURE WHERE id = ?;";
        try {
            readpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected Manufacture executeReadStatement(int key) throws SQLException {
        Manufacture m = new Manufacture();
        try {
            readpstm.setInt(1, key);
            ResultSet rs = readpstm.executeQuery();
            rs.next();
            m.setId(rs.getInt("ID"));
            m.setCarBrand(rs.getString("CAR_BRAND"));
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return m;
    }

    protected void createGetAllStatement() throws SQLException {
        String sql = "SELECT ID, Car_Brand FROM MANUFACTURE;";
        try {
            getAllpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected List executeGetAllStatement() throws SQLException {
        List<Manufacture> list = new ArrayList<>();
        try {
            ResultSet rs = getAllpstm.executeQuery();
            while (rs.next()) {
                Manufacture m = new Manufacture();
                m.setId(rs.getInt("ID"));
                m.setCarBrand(rs.getString("CAR_BRAND"));
                list.add(m);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return list;
    }
    @Override
    public void insert(Manufacture manufacture) throws DAOException {
        ResultSet rs;
        try {
            if (insertpstm == null) {
                createInsertStatement();
            }
            executeInsertStatement(manufacture);
        } catch (SQLException e) {
            throw new DAOException("fail on insert ");
        }
    }


    @Override
    public Manufacture read(int key) throws DAOException {

        try {
            if (readpstm == null) {
                createReadStatement();
            }

            return executeReadStatement(key);
        } catch (SQLException e) {
            throw new DAOException("fail on read ");
        }
    }

    @Override
    public void update(Manufacture manufacture) throws DAOException {
        try {
            if (updatepstm == null) {
                createUpdateStatement();
            }

            executeUpdateStatement(manufacture);
        } catch (SQLException e) {
            throw new DAOException("fail on update ");
        }
    }


    @Override
    public void delete(Manufacture manufacture) throws DAOException {
        try {
            if (deletepstm == null) {
                createDeleteStatement();
            }

            executeDeleteStatement(manufacture);
        }catch (SQLException e){
            throw new DAOException("fail on delete ");
        }
    }


    @Override
    public List<Manufacture> getAll() throws DAOException {
        List<Manufacture> list;
        try {
            if (getAllpstm == null) {
                createGetAllStatement();
            }
            list = executeGetAllStatement();

            return list;
        }catch (SQLException e){
            throw new DAOException("fail on getAll ");
        }
    }

    @Override
    public void close() throws DAOException{
        if (readpstm != null) {
            try {
                readpstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
        if (deletepstm != null) {
            try {
                deletepstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
        if (updatepstm != null) {
            try {
                updatepstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
        if (getAllpstm != null) {
            try {
                getAllpstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
        if (connection != null) {
            try {
                connection.close();

            } catch (Exception e) {
                throw new DAOException("fail on close");
            }
        }
    }

    public MySQLManufactureDao(Connection connection) {
        this.connection = connection;
    }
}


