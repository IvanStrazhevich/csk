package carsale.sql;

import carsale.dao.CarOrderDao;
import carsale.dao.DAOException;
import carsale.domain.CarOrder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Busik on 1/16/17.
 */
public class MySQLCarOrderDao implements CarOrderDao{
    private final Connection connection;
    private PreparedStatement readpstm = null;
    private PreparedStatement deletepstm = null;
    private PreparedStatement updatepstm = null;
    private PreparedStatement getAllpstm = null;
    private PreparedStatement insertpstm =null;

    protected  void createInsertStatement() throws SQLException {
        String sql = "insert into Car_Order (Car_ID, Manufacture_ID, First_Name, Second_Name) values (?,?,?,?);";
        try {
            insertpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeInsertStatement(CarOrder carOrder) throws SQLException {
        try {
            insertpstm.setInt(1, carOrder.getCarId());
            insertpstm.setInt(2, carOrder.getManufactureId());
            insertpstm.setString(3,carOrder.getFirstName());
            insertpstm.setString(4,carOrder.getSecondName());
            int count = insertpstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    protected void createDeleteStatement() throws SQLException {
        String sql = "DELETE FROM car_order WHERE id= ?;";
        try {
            deletepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeDeleteStatement(CarOrder carOrder) throws SQLException {
        try {
            deletepstm.setInt(1,carOrder.getId());
            int count = deletepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected  void createUpdateStatement() throws SQLException {
        String sql = "UPDATE CAR_ORDER SET CAR_ID = ?, " +
                "MANUFACTURE_ID = ?, FIRST_NAME = ?, SECOND_NAME = ?  WHERE ID = ?;";
        try {
            updatepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeUpdateStatement(CarOrder carOrder) throws SQLException {
        try {
            updatepstm.setInt(1, carOrder.getCarId());
            updatepstm.setInt(2, carOrder.getManufactureId());
            updatepstm.setString(3, carOrder.getFirstName());
            updatepstm.setString(4, carOrder.getSecondName());
            updatepstm.setInt(5, carOrder.getId());
            int count = updatepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    protected void createReadStatement() throws SQLException {
        String sql = "SELECT ID, CAR_ID, MANUFACTURE_ID, FIRST_NAME, SECOND_NAME" +
                " FROM CAR_ORDER WHERE id = ?;";
        try {
            readpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected CarOrder executeReadStatement(int key) throws SQLException {
        CarOrder co = new CarOrder();
        try {
            readpstm.setInt(1, key);
            ResultSet rs = readpstm.executeQuery();
            rs.next();
            co.setId(rs.getInt("ID"));
            co.setCarId(rs.getInt("CAR_ID"));
            co.setManufactureId(rs.getInt("MANUFACTURE_ID"));
            co.setFirstName(rs.getString("FIRST_NAME"));
            co.setSecondName(rs.getString("SECOND_NAME"));
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return co;
    }

    protected void createGetAllStatement() throws SQLException {
        String sql = "SELECT CO.ID, CO.CAR_ID, CO.MANUFACTURE_ID, CO.FIRST_NAME, CO.SECOND_NAME, M.Car_Brand, C.Model FROM CAR_ORDER CO LEFT OUTER JOIN Manufacture M on CO.Manufacture_ID = M.ID LEFT OUTER JOIN Car C on CO.Car_ID = C.ID;";
        try {
            getAllpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected List<CarOrder> executeGetAllStatement() throws SQLException {
        List <CarOrder> list = new ArrayList<>();
        try {
            ResultSet rs = getAllpstm.executeQuery();
            while (rs.next()) {
                CarOrder co = new CarOrder();
                co.setId(rs.getInt("ID"));
                co.setCarId(rs.getInt("CAR_ID"));
                co.setManufactureId(rs.getInt("MANUFACTURE_ID"));
                co.setFirstName(rs.getString("FIRST_NAME"));
                co.setSecondName(rs.getString("SECOND_NAME"));
                co.setCarBrand(rs.getString("CAR_BRAND"));
                co.setModel(rs.getString("MODEL"));
                list.add(co);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return list;
    }

    @Override
    public CarOrder read(int key) throws DAOException {

        try {
            if (readpstm == null) {
                createReadStatement();
            }

            return executeReadStatement(key);
        } catch (SQLException e) {
            throw new DAOException("fail on read");
        }
    }

    @Override
    public void update(CarOrder carOrder) throws DAOException {
        try {
            if (updatepstm == null) {
                createUpdateStatement();
            }

            executeUpdateStatement(carOrder);
        } catch (SQLException e) {
            throw new DAOException("fail on update");
        }
    }


    @Override
    public void delete(CarOrder carOrder) throws DAOException {
        try {
            if (deletepstm == null) {
                createDeleteStatement();
            }

            executeDeleteStatement(carOrder);
        }catch (SQLException e){
            throw new DAOException("fail on delete");
        }
    }

    @Override
    public List<CarOrder> getAll() throws DAOException {
        List<CarOrder> list;
        try {
            if (getAllpstm == null) {
                createGetAllStatement();
            }
            list = executeGetAllStatement();
            return list;

        }catch (SQLException e){
            throw new DAOException("Fail on getAll");
        }
    }
    @Override
    public void insert(CarOrder carOrder) throws DAOException {
        try {
            if (insertpstm == null) {
                createInsertStatement();
            }

            executeInsertStatement(carOrder);
        }catch (SQLException e){
            throw new DAOException("Fail on insert");
        }
    }



    @Override
    public void close()throws DAOException {
        if (readpstm != null) {
            try {
                readpstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        if (deletepstm != null) {
            try {
                deletepstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        if (updatepstm != null) {
            try {
                updatepstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        if (getAllpstm != null) {
            try {
                getAllpstm.close();
            } catch (SQLException e) {
                throw new DAOException("Fail on close");
            }
        }
        try{
            if (connection != null)
            {
            connection.close();}
        } catch (Exception e) {
            throw new DAOException("Fail on close");
        }

    }




    public MySQLCarOrderDao(Connection connection) {
        this.connection = connection;
    }
}

