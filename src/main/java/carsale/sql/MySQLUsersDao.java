package carsale.sql;

import carsale.dao.DAOException;
import carsale.dao.UsersDao;
import carsale.domain.Users;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by Busik on 3/15/17.
 */
public class MySQLUsersDao implements UsersDao {

    private final Connection connection;
    private PreparedStatement getAllpstm = null;
    private PreparedStatement insertpstm = null;
    private PreparedStatement deletepstm = null;

    protected void createDeleteStatement() throws SQLException {
        String sql = "DELETE FROM appusers WHERE id=?;";
        try {
            deletepstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeDeleteStatement(Users user) throws SQLException {
        try {
            deletepstm.setInt(1,user.getId());
            int count = deletepstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected  void createInsertStatement() throws SQLException {
        String sql = "insert into appusers (login, pass) value (?,?);";
        try {
            insertpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected void executeInsertStatement(Users user) throws SQLException {
        try {
            insertpstm.setString(1, user.getUser());
            insertpstm.setString(2, user.getPassword());
            int count = insertpstm.executeUpdate();
            if (count == 0) {
                throw new SQLException();
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    protected void createGetAllStatement() throws SQLException {
        try{
            String sql = "SELECT id, login, PASS FROM Appusers;";
            getAllpstm = connection.prepareStatement(sql);
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    protected List executeGetAllStatement() throws SQLException {
        List<Users> list = new ArrayList<>();
        try {
            ResultSet rs = getAllpstm.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                u.setId(rs.getInt("id"));
                u.setUser((rs.getString("login")));
                u.setPassword((rs.getString("pass")));
                list.add(u);
            }
        }catch (SQLException e) {
            throw new SQLException(e);
        }    return list;
    }
    @Override
    public List<Users> getAll() throws DAOException {
        List<Users> list;
        try {
            if (getAllpstm == null) {
                createGetAllStatement();
            }
            list = executeGetAllStatement();

            return list;
        }catch (SQLException e){
            throw new DAOException("fail on getAll ");
        }
    }
    @Override
    public void delete(Users user) throws DAOException {
        try {
            if (deletepstm == null) {
                createDeleteStatement();
            }
            executeDeleteStatement(user);
        }catch (SQLException e){
            throw new DAOException("fail on delete ");
        }
    }
    @Override
    public void insert(Users user) throws DAOException {

        try {
            if (insertpstm == null) {
                createInsertStatement();
            }
            executeInsertStatement(user);
        } catch (SQLException e) {
            throw new DAOException("fail on insert ");
        }
    }
    @Override
    public void close() throws DAOException{

        if (getAllpstm != null) {
            try {
                getAllpstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
        if (insertpstm != null) {
            try {
                insertpstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
        if (deletepstm != null) {
            try {
                deletepstm.close();
            } catch (SQLException e) {
                throw new DAOException("fail on close");
            }
        }
            if (connection != null){
                try{
                    connection.close();

        } catch (Exception e) {
                    throw new DAOException("fail on close");
        }
    }
    }


    public MySQLUsersDao(Connection connection) {
        this.connection = connection;
    }
}

