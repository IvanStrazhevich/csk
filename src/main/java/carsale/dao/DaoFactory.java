package carsale.dao;

/**
 * Фабрика объектов
 */


import java.sql.Connection;
import java.sql.SQLException;

public interface DaoFactory {
    /** Возвращает подключение к базе данных */
    public Connection getTestSQLConnection() throws DAOException;

    /** Возвращает подключение к базе данных */
    public Connection getSQLConnection() throws DAOException;

    /** Возвращает объект для управления персистентным состоянием объекта Car */
    public CarDao getCarDao() throws DAOException;

    /** Возвращает объект для управления персистентным состоянием объекта Manufacture */
    public ManufactureDao getManufactureDao() throws DAOException;

    /** Возвращает объект для управления персистентным состоянием объекта CarOrder */
    public CarOrderDao getCarOrderDao() throws DAOException;

    /** Возвращает объект для управления персистентным состоянием объекта Users */
    public UsersDao getUsersDao() throws DAOException;

}
