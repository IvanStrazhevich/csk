package carsale.dao;

/**
 * Created by Busik on 12/30/16.
 */

import carsale.domain.Manufacture;

import java.sql.ResultSet;
import java.util.List;

/** Объект для управления персистентным состоянием объекта Manufacture */
public interface ManufactureDao {

    /** Добавляет объект соответствующий записи в базу данных */
    public void insert(Manufacture manufacture) throws DAOException;

    /** Возвращает объект соответствующий записи с первичным ключом key или null */
    public Manufacture read(int key) throws DAOException;

    /** Сохраняет состояние объекта manufacture в базе данных */
    public void update(Manufacture manufacture) throws DAOException;

    /** Удаляет запись об объекте из базы данных */
    public void delete(Manufacture manufacture) throws DAOException;

    /** Возвращает список объектов соответствующих всем записям в базе данных */
    public List<Manufacture> getAll()throws DAOException;
    /** Удаляет все стейтментыперед закрытием приложения */
    public void close()throws DAOException;



}

