package carsale.domain;


import java.io.Serializable;
/**
 * Created by Busik on 12/30/16.
 * Объектное представление сущности Car
 */
public class Car implements Serializable {

    private Integer id;
    private Integer manufactureId;
    private String model;
    private Integer power;
    private Integer rangeKm;
    private Integer price;
    private Integer quantity;
    private String carBrand;


    public Integer getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getManufactureId() {
        return manufactureId;
    }
    public void setManufactureId(int manufactureId) {
        this.manufactureId = manufactureId;
    }
    public Integer getPower() {
        return power ;
    }
    public void setPower (int power) {
        this.power = power;
    }
    public Integer getRangeKm() {
        return rangeKm;
    }
    public void setRangeKm(int rangeKm) {
        this.rangeKm  = rangeKm;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public String getCarBrand() {
        return carBrand;
    }
    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }
}

