package carsale.domain;

/**
 * Created by Busik on 3/15/17.
 */
public class Users {
    private String user;
    private String password;
    private Integer id;

    public Integer getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
