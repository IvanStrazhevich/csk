package carsale.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import carsale.sql.*;

import java.sql.Connection;


/**
 * Created by Busik on 1/25/17.
 */
@WebListener
    public class CSKListener implements HttpSessionListener{

    public CSKListener(){

    }

        public void sessionCreated(HttpSessionEvent se) {

                try {

                    MySQLDaoFactory daoFactory = new MySQLDaoFactory();
                    se.getSession().setAttribute("SQLconnection", daoFactory.getSQLConnection());
                    se.getSession().setAttribute("SQLManufacture", new MySQLManufactureDao((Connection)
                            se.getSession().getAttribute("SQLconnection")));
                        se.getSession().setAttribute("SQLCar", new MySQLCarDao((Connection)
                                se.getSession().getAttribute("SQLconnection")));
                    se.getSession().setAttribute("SQLCarOrder", new MySQLCarOrderDao((Connection)
                            se.getSession().getAttribute("SQLconnection")));
                    se.getSession().setAttribute("SQLUsers", new MySQLUsersDao((Connection)
                            se.getSession().getAttribute("SQLconnection")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        public void sessionDestroyed(HttpSessionEvent se) {
            try {
                if (se.getSession().getAttribute("Login")!= null){
                    se.getSession().removeAttribute("Login");
                }
                MySQLManufactureDao manuDao = (MySQLManufactureDao) se.getSession().getAttribute("SQLManufacture");
                if (manuDao != null){
                    manuDao.close();
                }
                MySQLCarOrderDao carOrderDao = (MySQLCarOrderDao) se.getSession().getAttribute("SQLCarOrder");
                if (carOrderDao != null) {
                    carOrderDao.close();
                }
                MySQLCarDao carDao = (MySQLCarDao) se.getSession().getAttribute("SQLCar");
                if(carDao != null){
                    carDao.close();
                }
                MySQLUsersDao usersDao = (MySQLUsersDao) se.getSession().getAttribute("SQLUsers");
                if(usersDao != null){
                    usersDao.close();
                }
                Connection connection = (Connection) se.getSession().getAttribute("SQLconnection");
                if(connection != null){
                    connection.close();
                }

            } catch (Exception e){
                e.printStackTrace();
            }

        }
    }


